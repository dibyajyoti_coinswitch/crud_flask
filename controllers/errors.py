from flask import jsonify, Blueprint

errors_blueprint = Blueprint('errors', __name__)


@errors_blueprint.app_errorhandler(500)
def handle_500(ex):
    print("ERROR")
    print(ex)
    return jsonify(ex=ex), 500
