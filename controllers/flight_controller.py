from flask import request, jsonify, Blueprint, current_app

from Models.Flights import Flights

db_session = current_app.config.db_session
flights_bp = Blueprint('flights_bp', __name__)


@flights_bp.route("/", methods=["GET"])
def fetch_all_flights():
    all_flights = db_session.query(Flights).all()
    flights = [
        {
            "id": x.id,
            "origin": x.origin,
            "destination": x.destination,
            "duration": x.duration
        } for x in all_flights]
    return jsonify(success=True, flights=flights), 201


@flights_bp.route("/", methods=["POST"])
def create_flight():
    data = request.get_json()
    print(data)
    flight = Flights(
        origin=data["origin"],
        destination=data["destination"],
        duration=data["duration"]
    )
    db_session.add(flight)
    db_session.commit()
    response_object = {
        'status': 'success',
        'message': 'Flight created.',
    }
    return response_object


@flights_bp.route("/<id>", methods=["GET"])
def get_flight(id):
    data = request.get_json()
    db_session.query(Flights).filter(Flights.id == id).update({
        **data
    })
    db_session.commit()
    return {
        'status': 'success',
        'message': 'data updated!'
    }


@flights_bp.route("/<id>", methods=["PUT"])
def update_flight(id):
    data = request.get_json()
    db_session.query(Flights).filter(Flights.id == id).update({
        **data
    })
    db_session.commit()
    return {
        'status': 'success',
        'message': 'data updated!'
    }


@flights_bp.route("/<id>", methods=["DELETE"])
def delete_flight(id):
    db_session.query(Flights).filter(Flights.id == id).delete()
    db_session.commit()
    return {
        'status': 'success',
        'message': 'data deleted!'
    }


