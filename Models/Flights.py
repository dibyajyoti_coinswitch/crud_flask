from sqlalchemy import Column, Integer, String, Sequence, Float,PrimaryKeyConstraint, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.dialects.postgresql import UUID

import uuid

from Models import Base


class Flights(Base):
    __tablename__ = "flights"
    id = Column(
        Integer,
        unique=True,
        nullable=False,
        primary_key=True
    )
    origin = Column(String)
    destination = Column(String)
    duration = Column(Integer)