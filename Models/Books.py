from sqlalchemy import Column, Integer, String, Sequence, Float,PrimaryKeyConstraint, ForeignKey
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship

from Models import Base
from Models.Flights import Flights
import uuid


class Books(Base):
    __tablename__ = "books"
    id = Column(
        UUID(as_uuid=True),
        default=uuid.uuid4(),
        unique=True,
        nullable=False,
        primary_key=True
    )
    flight_id = Column(Integer, ForeignKey("flights.id"))
    name = Column(String)
    flight = relationship(Flights)
