from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker


def engine(config):
    return create_engine(name_or_url=config.get("SQLALCHEMY_DATABASE_URI"))


def db(config):
    created_engine = engine(config)
    return scoped_session(sessionmaker(bind=created_engine))
