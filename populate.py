from db import db

db = db({'SQLALCHEMY_DATABASE_URI': 'postgresql://localhost/book_store'})


def create_table(database: object) -> object:
    database.execute(
        """CREATE TABLE flights
        (
            id SERIAL PRIMARY KEY,
            origin VARCHAR NOT NULL,
            destination VARCHAR NOT NULL,
            duration INTEGER NOT NULL
        );"""
    )
    db.commit()
    return db.close()


def populate_table(database: object):
    database.execute(
        """
        INSERT INTO flights (
            origin, destination, duration
        )
        VALUES ('New York', 'London', 415);
        """
    )
    database.execute("INSERT INTO flights (origin, destination, duration) VALUES ('Shanghai', 'Paris', 760);")
    database.execute("INSERT INTO flights (origin, destination, duration) VALUES ('Istanbul', 'Tokyo', 700);")
    database.execute("INSERT INTO flights (origin, destination, duration) VALUES ('New York', 'Paris', 435);")
    db.commit()
    return db.close()


def fetch_query(database):
    return database.execute("SELECT * FROM flights")


# create_table(db)
populate_table(db)
print(fetch_query(db))
