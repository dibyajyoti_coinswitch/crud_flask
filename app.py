from dotenv import load_dotenv
import os
from flask import Flask, g, jsonify
from db import db, engine
from Models import create_all_tables

load_dotenv()
app: Flask = Flask(__name__)


env_config = os.environ['CONFIGURATION_SETUP']
app.config.from_object(env_config)
session = db(app.config)

if __name__ == "__main__":
    with app.app_context():
        # adding db session to app config,
        # as values added in g are not available in blueprints
        g.db_session = session
        app.config.db_session = session
        create_all_tables(engine(app.config))

        from controllers import flights_bp
        from controllers.errors import errors_blueprint

        app.register_blueprint(flights_bp, url_prefix="/flights")
        app.register_blueprint(errors_blueprint)

        app.run(port=app.config.get("PORT"), debug=app.config.get("DEBUG"))
